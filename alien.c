#include "logger.h"
#include "commons.h"
#include "globals.h"
#include "console.h"
#include "alien.h"
#include "bullet.h"
#include "llist.h"
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>

void *alienThread(void *alienInfo) {

    // Initialize alien coord thread
	alienVar *a = (alienVar*) alienInfo;
	pthread_t self = pthread_self();
	pthread_mutex_init(&a->point_mutex, NULL);

    pthread_mutex_lock(&logger_mutex);
    writeToLog_threaded("Alien", self, "Thread created.");
    pthread_mutex_unlock(&logger_mutex);

    // Set alien's initial values
	pthread_mutex_lock(&a->point_mutex);
		a->point->x = 0;
		a->point->y = 0;
		a->dead = false;
	pthread_mutex_unlock(&a->point_mutex);

    // Determine speed by setting random sleep time
	int ticks = (int) rand() % 10 + 20;
    int moveMod = 2; // How much to move the alien by each tick
    alienState state = OPEN;

    // Draw initial self to screen
	pthread_mutex_lock(&image_mutex);
    screenDrawImage(a->point->x, a->point->y, alienOpenModel, 2);
	pthread_mutex_unlock(&image_mutex);

	// Start alien move cycle, only stop when alien is dead
	while(a->dead == false) {
		
        // Clear current self from image
        if (a->point->x > 1) {
	        pthread_mutex_lock(&image_mutex);
            screenClearImage(a->point->y, a->point->x, 2, 2);
	        pthread_mutex_unlock(&image_mutex);
        }

        // If at edge of screen, move down and negate moveMod. Otherwise
        //   keep moving forward
        if (((a->point->x < COORD_X-1 && moveMod > 0) || (a->point->x > 1 && moveMod < 0)) && a->dead == false) {
          
            pthread_mutex_lock(&a->point_mutex);
            a->point->x += moveMod;
            pthread_mutex_unlock(&a->point_mutex);

            // Draw self to image
            if (state == OPEN) {
            	
                pthread_mutex_lock(&image_mutex);
                screenDrawImage(a->point->y, a->point->x, alienOpenModel, 2);
	            pthread_mutex_unlock(&image_mutex);
                
                state = CLOSED;
            } else {
	            
                pthread_mutex_lock(&image_mutex);
                screenDrawImage(a->point->y, a->point->x, alienClosedModel, 2);
	            pthread_mutex_unlock(&image_mutex);
                
                state = OPEN;
            }
            
        } else if (a->dead == false){
            
            pthread_mutex_lock(&logger_mutex);
            writeToLog_threaded("Alien", self, "Reverse direction.");
            pthread_mutex_unlock(&logger_mutex);

            // Reverse direction
            moveMod *= -1;
            
            // Move down
            if (a->point->y + 2 < COORD_Y && a->dead == false) {
                
                pthread_mutex_lock(&a->point_mutex);
                a->point->y += 2;
                pthread_mutex_unlock(&a->point_mutex);
                
                if (state == OPEN) {
	                
                    pthread_mutex_lock(&image_mutex);
                    screenDrawImage(a->point->y, a->point->x, alienOpenModel, 2);
	                pthread_mutex_unlock(&image_mutex);
                    
                    state = CLOSED;
                } else {
	                
                    pthread_mutex_lock(&image_mutex);
                    screenDrawImage(a->point->y, a->point->x, alienClosedModel, 2);
	                pthread_mutex_unlock(&image_mutex);
                    
                    state = OPEN;
                }
            }
        }
        // Draw bullet
        if (rand()%8 == 0 && a->dead == false) {

            pthread_mutex_lock(&logger_mutex);
            writeToLog_threaded("Alien", self, "Shot bullet.");
            pthread_mutex_unlock(&logger_mutex);

        	Point *bulletPoint = malloc(sizeof(Point));
        	bulletPoint->x = a->point->x;
        	bulletPoint->y = a->point->y + 2;
            createAlienBulletThread(bulletPoint);
        }
    
        if (a->dead == false)
            sleepTicks(ticks);
    }

    pthread_mutex_lock(&logger_mutex);
    writeToLog_threaded("Alien", self, "Alien dead.");
    pthread_mutex_unlock(&logger_mutex);

	// Draw dead alien to screen
	pthread_mutex_lock(&image_mutex);
	screenDrawImage(a->point->y, a->point->x, alienDeadModel, 2);
	pthread_mutex_unlock(&image_mutex);

    pthread_mutex_lock(&logger_mutex);
    writeToLog_threaded("Alien", self, "Removed from active aliens.");
    pthread_mutex_unlock(&logger_mutex);

	// Remove alien from list of aliens
	pthread_mutex_lock(&alienThreads_mutex);
	removeNode(self, alienThreads);
	pthread_mutex_unlock(&alienThreads_mutex);

	pthread_exit(NULL);
}

void *alienControllerThread() {

	int rc;				// pthread_create return code
	pthread_t alien;	// Thread for new alien
	
	// Seed random num gen
	srand(time(NULL));

    // Sleep for a random amount of time, wake up and spawn an alien.
    //	until game is over
	while(*gameOver == false) {
		
		sleepTicks((int) rand()%150 + 375);	

        pthread_mutex_lock(&logger_mutex);
        writeToLog("Alien Controller", "Creating alien.");
        pthread_mutex_unlock(&logger_mutex);

		// Create a new alien thread. Then check for any errors
		alienVar *a = malloc(sizeof(alienVar));
        a->point = malloc(sizeof(Point));
		
        void *ptr = a;
		rc = pthread_create(&alien, NULL, alienThread, ptr);
		if (rc) {
//            writeToLog("Alien Controller", "ERROR. Return code from alienControllerThread() pthread_create() is %d\n", rc);
			exit(-1);
		}

		// Lock alienThreads, then store new alien thread in Linked List
		pthread_mutex_lock(&alienThreads_mutex);
		add(a, alien, alienThreads);
		pthread_mutex_unlock(&alienThreads_mutex);
	}

	// Kill all aliens after game is over
	pthread_mutex_lock(&alienThreads_mutex);
	clearList(alienThreads);
	pthread_mutex_unlock(&alienThreads_mutex);

	pthread_exit(NULL);
}
