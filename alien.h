#ifndef ALIEN_H
#define ALIEN_H

#include <pthread.h>
#include "commons.h"

// TODO rename alienVar to AlienInfo

//
// alienVar
//   contains properties for alien threads
// @var coordX- used for current position
// @var coordY- 
// @var dead- flag is set to let alien know it was shot
// @var coord_mutex-locks alien values
//
typedef struct ALIENVAR alienVar;

struct ALIENVAR {
	Point *point;
	bool dead;
	pthread_mutex_t point_mutex;
};

//
// Alien states
//
typedef enum {OPEN, CLOSED, DEAD} alienState;
char *alienOpenModel[2];
char *alienClosedModel[2];
char *alienDeadModel[2];


//
// alienControllerThread
//   Spawns aliens until game is over. After each alien thread is created, controller
//   sleeps for a random amount of time. Every alien thread is added to the alienThreads
//   linked list.
//
void *alienControllerThread();

//
// alienThread
//   Spawns bullets randomly and moves accross screen (left or right) until either the
//   alien reaches the edge of the screen, or the alien is shot. All bullet threads are
//   added to the bulletThreads linked list.
// @param a -alien thread's information (alienVar)
//
void *alienThread(void *a);

#endif
