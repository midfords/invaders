#include "logger.h"
#include "commons.h"
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

bool openLog(char* fileName) {

	if (fileName == NULL)
		return false;

	file = fopen(fileName, "ab+");
	if (file == NULL)
		return false;

	return true;
}

bool closeLog() {

	if (file == NULL)
		return false;

	fclose(file);
	return true;
}

//TODO include timestamp
bool writeToLog_threaded(char* entity, pthread_t thread, char* message) {

	if (file == NULL) 
		return false;

	fprintf(file, "[%s - %d] %s\n", entity, (int)thread, message);
	fflush(file);

	return true;
}

bool writeToLog(char* entity, char* message) {

	if (file == NULL)
		return false;

	fprintf(file, "[%s] %s\n", entity, message);
	fflush(file);

	return true;
}
