#ifndef COMMONS_H
#define COMMONS_H

//
// Defines the bool type
//
typedef enum {false, true} bool;

//
// Defines the point struct
//
typedef struct POINT Point;
struct POINT {
    int x;
    int y;
};

#endif
