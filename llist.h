#ifndef LLIST_H
#define LLIST_H

#include <pthread.h>
#include "alien.h"
#include "commons.h"

// 
// Node
//   Defines the node type
// @var thread -the thread being held by the node
// @var val -the information for aliens
// @var next -the next node in linked list
//
typedef struct NODE Node;

struct NODE {
	alienVar *val;
	pthread_t thread;
	Node *next;
};

//
// LList
//   Defines the linked list type
// @var head -the first node pointed to be the linked list
//
typedef struct LLIST LList;

struct LLIST {
	Node *head;
};

//
// add
//   Adds to a linked list (function does not account for mutex)
// @param nthread -the thread to add to the linked list
// @param list -the list to add a new thread to
// @param a -contains alien parameters. This is set to NULL for other lists
//
void add(alienVar *a, pthread_t nthread, LList *list);

//
// remove
//   Removes a node based on given thread from the linked list. adds
//   thread to garbageThreads
// @param thread -thread to remove from linked list
// @param list -list to remove thread from
//
void removeNode(pthread_t thread, LList *list);

//
// removeHead
//   Removes and returns the head of a linked list. Returns NULL if list is empty
// @param list -list to remove head from
//
Node* getHead(LList *list);

//
// bulletDetection
//   Traverses list to find matching alien, then returns thread id.
//   Returns -1 if not found.
//
bool bulletDetection(Point *p, LList *list);
bool hitAlien(Point *p, Point *a);

//
// clearList
//   Removes all nodes from a linked list and adds all threads to
//   garbageThreads
// @param list -the list to clear
//
void clearList(LList *list);

//
// clearAux
//   A recursive method that clears a linked list and adds all threads
//   to garbageThreads
// @param curr -the current node
//
void clearAux(Node *curr);

#endif
