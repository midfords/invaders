#ifndef BULLET_H
#define BULLET_H

#include "commons.h"

//
// Alien bullet model
//
char *alienBulletModel;
char *playerBulletModel;

//
// alienBulletThread
//   Runs an alien's bullet, and makes checks to see if player was hit.
// @param p- pointer to point struct for starting pos
//
void *alienBulletThread(void *p);

//
// hitPlayer
//   Checks if the player was hit, returns true or false.
// @param p- pointer to point of alien bullet.
//
bool hitPlayer(Point *p);

//
// playerBulletThread
//   Runs a player's bullets. After every move, list of aliens is checked for hits.
// @param p-pointer to point struct for starting pos
//
void *playerBulletThread(void *p);

//
// createPlayerThread
//   Initilizes a new bullet thread.
// @param x-used for starting pos
// @param y
//
void createPlayerBulletThread(Point *p);

//
// createAlienThread
//   Initilizes a new bullet thread.
// @param x-used for starting pos
// @param y
//
void createAlienBulletThread(Point *p);

#endif
