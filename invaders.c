#define _GNU_SOURCE

#include "logger.h"
#include "invaders.h"
#include "garbage.h"
#include "console.h"
#include "globals.h"
#include "player.h"
#include "llist.h"
#include "alien.h"
#include "bullet.h"
#include "commons.h"
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

void initializeVariables(Point *screenDim) {

	writeToLog("Invaders Setup", "Initializing global variables.");

	pthread_mutexattr_t attributes;
	pthread_mutexattr_init(&attributes);
	pthread_mutexattr_settype(&attributes, PTHREAD_MUTEX_NORMAL);
	
	// Set up alientThreads_mutex
	pthread_mutex_init(&alienThreads_mutex, &attributes);

	// Set up bulletThreads_mutex
	pthread_mutex_init(&bulletThreads_mutex, &attributes);

    // Set up garbageThreads_mutex
    pthread_mutex_init(&garbageThreads_mutex, &attributes);

	// Set up player_mutex
	pthread_mutex_init(&player_mutex, &attributes);

	// Set up logger_mutex
	pthread_mutex_init(&logger_mutex, &attributes);

	// Set up player coords
    p_point = malloc(sizeof(Point));
	p_point->x = 0;
	p_point->y = 0;

	// Set up alien list
	alienThreads = malloc(sizeof(LList));
	alienThreads->head = NULL;	

	// Set up bullet list
	bulletThreads = malloc(sizeof(LList));
	bulletThreads->head = NULL;

    // Set up garbage list
    garbageThreads = malloc(sizeof(LList));
    garbageThreads->head = NULL;

    // Initialize gmae models
    alienOpenModel[0] = malloc(sizeof(char)*2);
    alienOpenModel[1] = malloc(sizeof(char)*2);
    alienOpenModel[0][0] = '0';
    alienOpenModel[0][1] = 'o';
    alienOpenModel[1][0] = '/';
    alienOpenModel[1][1] = '\\';

    alienClosedModel[0] = malloc(sizeof(char)*2);
    alienClosedModel[1] = malloc(sizeof(char)*2);
    alienClosedModel[0][0] = 'o';
    alienClosedModel[0][1] = '0';
    alienClosedModel[1][0] = '\\';
    alienClosedModel[1][1] = '/';

    alienDeadModel[0] = malloc(sizeof(char)*2);
    alienDeadModel[1] = malloc(sizeof(char)*2);
    alienDeadModel[0][0] = 'X';
    alienDeadModel[0][1] = 'X';
    alienDeadModel[1][0] = '/';
    alienDeadModel[1][1] = '\\';
   
    alienBulletModel = malloc(sizeof(char)*2);
    alienBulletModel[0] = 'v';
    alienBulletModel[1] = 'v';

    playerBulletModel = malloc(sizeof(char)*2);
    playerBulletModel[0] = '|';
    playerBulletModel[1] = '|';

    playerModel[0] = malloc(sizeof(char)* 2);
	playerModel[1] = malloc(sizeof(char)* 2);
	playerModel[0][0] = '/';
	playerModel[0][1] = '\\';
	playerModel[1][0] = '<';
	playerModel[1][1] = '>';

	// Set up image of gameboard
	int i;
	for (i = 0; i < COORD_Y; i++) {
		image[i] = malloc(sizeof(char) * COORD_X);
		int j;
		for (j = 0; j < COORD_X; j++) {
            if (i == 2)
                image[i][j] = '-';
            else
                image[i][j] = ' ';
		}
	}

	// Initialize garbage collector active toggle
	garbageCollectorActive = malloc(sizeof(bool));
	*garbageCollectorActive = true;
	
	// Initialize game over
	gameOver = malloc(sizeof(bool));
	*gameOver = false;

	// Initialize life counter to 3
	lifeCounter = malloc(sizeof(int));
	*lifeCounter = 3;
}

// nope.
void detectScreenDim(Point *point) {

	writeToLog("Invaders Setup", "Detecting screen dimensions.");

	point->x = COORD_X;
	point->y = COORD_Y;
}

void *screenRefresher() {

	// Refresh then sleep
	while (*gameOver == false) {

		// Call console refresh
		pthread_mutex_lock(&image_mutex);
		screenRefresh();
		pthread_mutex_unlock(&image_mutex);

		sleepTicks(1);
	}
	pthread_exit(NULL);
}

void invadersRun() {

	pthread_t screenRefreshThread;	// Calls console.c refresh function, then sleeps
	pthread_t acThread;				// Starts thread for alien controller
	pthread_t pcThread;				// Starts thread for player controller
//	pthread_t gcThread;				// Starts thread for garbage collector
	int rc;							// pthread_create return code
	Point *screenDim;				// Size of screen dimensions
    
    screenDim = malloc(sizeof(Point));

    // Initialize globals
	detectScreenDim(screenDim);
	initializeVariables(screenDim);

	if (!screenInit(screenDim->y, screenDim->x, image)) {
		writeToLog("Invaders Run", "ERROR. Screen dim wrong. Exiting game.");
		exit(-1);
	}

	pthread_mutex_lock(&logger_mutex);
	writeToLog("Invaders Setup", "Starting screen refresh thread.");
	pthread_mutex_unlock(&logger_mutex);

	// Start screen refresh thread
	rc = pthread_create(&screenRefreshThread, NULL, screenRefresher, NULL);
	if (rc) {
//		writeToLog("Invaders Run", "ERROR. Return code from invadersRun() pthread_create() is %d\n", rc);
		exit(-1);
	}

	pthread_mutex_lock(&logger_mutex);
	writeToLog("Invaders Setup", "Starting player controller thread.");
	pthread_mutex_unlock(&logger_mutex);

	// Start player control thread
	rc = pthread_create(&pcThread, NULL, playerControllerThread, NULL);
	if (rc) {
//		writeToLog("Invaders Run", "ERROR. Return code from invadersRun() pthread_create() is %d\n", rc);
		exit(-1);
	}

	pthread_mutex_lock(&logger_mutex);
	writeToLog("Invaders Setup", "Starting alien controller thread.");
	pthread_mutex_unlock(&logger_mutex);

	// Start alien control thread
	rc = pthread_create(&acThread, NULL, alienControllerThread, NULL);
	if (rc) {
//		writeToLog("Invaders Run", "ERROR. Return code from invadersRun() pthread_create() is %d\n", rc);
		exit(-1);
	}

//	pthread_mutex_lock(&logger_mutex);
//	writeToLog("Invaders Setup", "Starting garbage collection thread.");
//	pthread_mutex_unlock(&logger_mutex);

	// Start garbage collection thread
//	rc = pthread_create(&gcThread, NULL, garbageCollectionThread, NULL);
//	if (rc) {
//		printf("ERROR. Return code from invadersRun() pthread_create() is %d\n", rc);
//		exit(-1);
//	}

	// Clean up globals
    free(screenDim);
	pthread_join(acThread, NULL);
    pthread_join(pcThread, NULL);
//    pthread_join(gcThread, NULL);
    pthread_join(screenRefreshThread, NULL);
    free(alienOpenModel);
    free(alienClosedModel);
    free(alienDeadModel);
    free(playerModel);
    free(alienBulletModel);
    free(playerBulletModel);
    free(image);
    free(p_point);
    free(alienThreads);
    free(bulletThreads);
    free(garbageThreads);
    free(gameOver);
    free(garbageCollectorActive);
    free(lifeCounter);
}
