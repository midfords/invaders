#ifndef GLOBALS_H
#define GLOBALS_H

#include "commons.h"
#include "llist.h"
#include <pthread.h>

// Board dimensions
#define COORD_X 110
#define COORD_Y 30	
#define SAFE_ZONE 5	// Amount of space player can move in

//
// Linked list for all alien threads
//
LList *alienThreads;

//
// Linked list for all bullet threads
//
LList *bulletThreads;

//
// Linked list of all threads that are to be destroyed
//
LList *garbageThreads;

// 
// Mutex for alientThreads
//
pthread_mutex_t alienThreads_mutex;

//
// Mutex for bulletThreads
//
pthread_mutex_t bulletThreads_mutex;

//
// Mutex for garbageThreads
//
pthread_mutex_t garbageThreads_mutex;

//
//
//
bool *garbageCollectorActive;

//
// Complete game board image. Bullets, aliens and players write to this. Then the
//   board is printed to console
//
char *image[COORD_Y];

//
// Mutex for image. Many threads must alter this global var.
//
pthread_mutex_t image_mutex;

//
// Player's x and y position
//
Point *p_point;

//
// Mutex for player coords
//
pthread_mutex_t player_mutex;

//
// If the game is over, the flag is changed so all threads know
//
bool *gameOver;

//
// Counter for number of lives in game. Game is over when count is 0
//
int *lifeCounter;

//
// Mutex for logger. Prevents mixed threaded output.
//
pthread_mutex_t logger_mutex;

#endif
