
#include "invaders.h"
#include "logger.h"
#include <stdio.h>
#include <stdlib.h>

void setDebugFlags(int argc, char**argv) {

	int i;
	for (i = 0; i < argc; i++) {
		switch((int)argv[i]) {
			case (int)'a':
				// Turn off aliens
				break;
			case (int)'p':
				// Turn off player
				break;
			case (int)'w':
				// Turn off player bullets
				break;
			case (int)'b':
				// Turn off alien bullets
				break;
		}
	}
}

int main(int argc, char**argv) {

	setDebugFlags(argc, argv);

	if(!openLog("invaders.log"))
		printf("ERROR. Could not open logger.");

	writeToLog("Main", "----------------[Starting invaders]----------------");

	invadersRun();

	writeToLog("Main", "Exit successful!");
    closeLog();

	return 0;
}
