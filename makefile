
CC = gcc
CFLAGS = -Wall -g

PROG = ./invaders

HDRS = alien.h commons.h bullet.h console.h globals.h invaders.h llist.h player.h garbage.h logger.h
SRCS = alien.c bullet.c console.c invaders.c llist.c main.c player.c garbage.c logger.c
OBJS = alien.o bullet.o console.o invaders.o llist.o main.o player.o garbage.o logger.o

$(PROG): $(OBJS)
	$(CC) $(CFLAGS) $(OBJS) -o $(PROG) -lcurses

main.o: main.c $(HDRS)
	$(CC) $(CFLAGS) -c main.c -o main.o

alien.o: alien.c $(HDRS)
	$(CC) $(CFLAGS) -c alien.c -o alien.o

bullet.o: bullet.c $(HDRS)
	$(CC) $(CFLAGS) -c bullet.c -o bullet.o

console.o: console.c $(HDRS)
	$(CC) $(CFLAGS) -c console.c -o console.o

invaders.o: invaders.c $(HDRS)
	$(CC) $(CFLAGS) -c invaders.c -o invaders.o

llist.o: llist.c $(HDRS)
	$(CC) $(CFLAGS) -c llist.c -o llist.o

player.o: player.c $(HDRS)
	$(CC) $(CFLAGS) -c player.c -o player.o

garbage.o: garbage.c $(HDRS)
	$(CC) $(CFLAGS) -c garbage.c -o garbage.o

logger.o: logger.c $(HDRS)
	$(CC) $(CFLAGS) -c logger.c -o logger.o

clean:
	rm $(OBJS) $(PROG)