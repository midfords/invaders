#include "logger.h"
#include "commons.h"
#include "console.h"
#include "globals.h"
#include "garbage.h"
#include "llist.h"
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>

void *garbageCollectionThread() {

	// Constantly join dead threads. If there are no threads, sleep for awhile.
	while(*garbageCollectorActive == true) {

		pthread_mutex_lock(&garbageThreads_mutex);
		Node* n = getHead(garbageThreads);
		pthread_mutex_lock(&garbageThreads_mutex);

		if (n != NULL) {
			
			pthread_join(n->thread, NULL);
			
			pthread_mutex_lock(&logger_mutex);
//			writeToLog("Garbage Collector", "Joined thread %d.", n->thread);
			pthread_mutex_unlock(&logger_mutex);

			free(n->val);
			free(n);

			pthread_mutex_lock(&logger_mutex);
			writeToLog("Garbage Collector", "Freed node in memory.");
			pthread_mutex_unlock(&logger_mutex);

			n = getHead(garbageThreads);

		} else {

			pthread_mutex_lock(&logger_mutex);
			writeToLog("Garbage Collector", "garbageThreads is empty. Sleeping.");
			pthread_mutex_unlock(&logger_mutex);

			sleepTicks(10);
		}
	}

	pthread_exit(NULL);
}