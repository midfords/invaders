#ifndef INVADERS_H
#define INVADERS_H

#include "commons.h"

//
// initializeVariables
//   Initializes mutex's for alien and bullet linked lists
//
void initializeVariables(Point *p);

//
// detectScreenDim
//   Detects and returns the dimensions for the console
// @param x -width of console
// @param y -height of console
//
void detectScreenDim(Point *p);

//
// screenRefresher
//   Thread that calls screenRefresh, then sleeps until next refresh
//
void *screenRefresher();

//
// invadersRun
//   Initializes the invaders game by starting controller threads
//
void invadersRun();

#endif
