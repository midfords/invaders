#include "commons.h"
#include "logger.h"
#include "globals.h"
#include "console.h"
#include "bullet.h"
#include "llist.h"
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>

void *alienBulletThread(void *point) {
	
	// add self to llist
	pthread_t self = pthread_self();
	pthread_mutex_lock(&bulletThreads_mutex);
	add(NULL, self, bulletThreads);
	pthread_mutex_unlock(&bulletThreads_mutex);
	
	pthread_mutex_lock(&logger_mutex);
    writeToLog_threaded("Alien Bullet", self, "Thread created.");
    pthread_mutex_unlock(&logger_mutex);

	int ticks = 10;
	Point *p = point;
	bool dead = false;

    // Draw initial bullet to screen
    pthread_mutex_lock(&image_mutex);
    screenDrawImage(p->y, p->x, &alienBulletModel, 1);
    pthread_mutex_lock(&image_mutex);

	// Loop until bullet is off screen or hits player
	while (p->y < COORD_Y && dead == false) {
		
		// Draw bullet to screen
		pthread_mutex_lock(&image_mutex);
		if (p->y > 0)
			screenClearImage(p->y, p->x, 2, 1);
        p->y += 1;
        screenDrawImage(p->y, p->x, &alienBulletModel, 1);
		pthread_mutex_unlock(&image_mutex);


		// Check if player was hit aka is infront of bullet
		if (hitPlayer(p)) {

			pthread_mutex_lock(&logger_mutex);
    		writeToLog_threaded("Alien Bullet", self, "Player hit.");
    		pthread_mutex_unlock(&logger_mutex);

			*gameOver = true;
			dead = true;
			break;
		}

		// Sleep
		if(dead == false)
			sleepTicks(ticks);
	}

    pthread_mutex_lock(&logger_mutex);
    writeToLog_threaded("Alien Bullet", self, "Bullet gone.");
    pthread_mutex_unlock(&logger_mutex);

	// Clear final image of bullet
	pthread_mutex_lock(&image_mutex);
	screenClearImage(p->y, p->x, 2, 1);
	pthread_mutex_unlock(&image_mutex);

	// Remove bullet from list
	pthread_mutex_lock(&bulletThreads_mutex);
	removeNode(self, bulletThreads);
	pthread_mutex_unlock(&bulletThreads_mutex);
	
    free(p);
	pthread_exit(NULL);
}

bool hitPlayer(Point *p) {

		// Conditions for hit detection
		int x1 = p->x + 1;
        int y1 = p->y - 1;
		int x2 = p->x;
        int y2 = p->y - 1;
		int x3 = p->x - 1;
        int y3 = p->y - 1;

        return (p_point->x == x1 && p_point->y == y1)
		    || (p_point->x == x2 && p_point->y == y2)
		    || (p_point->x == x3 && p_point->y == y3);
}

void *playerBulletThread(void *point) {

	// Draw to screen init
	pthread_t self = pthread_self();
	pthread_mutex_lock(&bulletThreads_mutex);
	add(NULL, self, bulletThreads);
	pthread_mutex_unlock(&bulletThreads_mutex);	

	pthread_mutex_lock(&logger_mutex);
    writeToLog_threaded("Player Bullet", self, "Thread created.");
    pthread_mutex_unlock(&logger_mutex);

	int ticks = 5;
	Point *p = point;
	bool dead = false;

    // Draw initial bullet
    pthread_mutex_lock(&image_mutex);
    screenDrawImage(p->y, p->x, &playerBulletModel, 1);
    pthread_mutex_unlock(&image_mutex);

	// Loop until off screen or dead
	while(p->y >= 0 && dead == false) {

        // Check space above for alien
        pthread_mutex_lock(&alienThreads_mutex);
        bool hit = bulletDetection(p, alienThreads);
        pthread_mutex_unlock(&alienThreads_mutex);

        // If alien was hit
        if (hit) {
        	pthread_mutex_lock(&logger_mutex);
		    writeToLog_threaded("Player Bullet", self, "Alien hit.");
		    pthread_mutex_unlock(&logger_mutex);

            dead = true;
            break;
        }

		// Draw to screen
		pthread_mutex_lock(&image_mutex);
		if (p->y < COORD_Y - 3)
			screenClearImage(p->y, p->x, 2, 1);
        p->y -= 1;
		screenDrawImage(p->y, p->x, &playerBulletModel, 1);
		pthread_mutex_unlock(&image_mutex);

		// Sleep if not dead
		if (dead == false)
			sleepTicks(ticks);
	}

	// Clear self from screen if not dead
	if (dead == false) {

	    pthread_mutex_lock(&logger_mutex);
	    writeToLog_threaded("Player Bullet", self, "Bullet gone.");
	    pthread_mutex_unlock(&logger_mutex);

		pthread_mutex_lock(&image_mutex);
		screenClearImage(p->y, p->x, 2, 1);
		pthread_mutex_unlock(&image_mutex);

	} else {

		pthread_mutex_lock(&logger_mutex);
	    writeToLog_threaded("Player Bullet", self, "Bullet dead.");
	    pthread_mutex_unlock(&logger_mutex);
	}

	// Remove self from list of bullets
	pthread_mutex_lock(&bulletThreads_mutex);
	removeNode(self, bulletThreads);
	pthread_mutex_unlock(&bulletThreads_mutex);

    free(p);
	pthread_exit(NULL);
}


void createPlayerBulletThread(Point *p) {

	int rc;
	pthread_t bullet;

	// Create bullet thread
	rc = pthread_create(&bullet, NULL, playerBulletThread, p);
	if (rc) {
		printf("ERROR. Return code from createPlayerBulletThread() pthread_create() is %d\n", rc);
		exit(-1);
	}
}

void createAlienBulletThread(Point *p) {

	int rc;
	pthread_t bullet;
	
	//create bullet thread
	rc = pthread_create(&bullet, NULL, alienBulletThread, p);
	if (rc) {
		printf("ERROR. Return code from createPlayerBulletThread() pthread_create() is %d\n", rc);
		exit(-1);
	}
}


