#include "llist.h"
#include "logger.h"
#include "commons.h"
#include "globals.h"
#include <stdio.h>
#include <stdlib.h>

void add(alienVar *a, pthread_t nthread, LList *list) {

	pthread_mutex_lock(&logger_mutex);
    writeToLog_threaded("Linked List", nthread, "Added to List.");
    pthread_mutex_unlock(&logger_mutex);

	// Create a new node
	Node *newNode = malloc(sizeof(Node));

    // Set values and add to list
    newNode->val = a;
	newNode->thread = nthread;
	newNode->next = list->head;

	list->head = newNode;
}

bool bulletDetection(Point *p, LList *list) { 

    if (list == NULL || list->head == NULL)
        return false;
    
	Node *curr = list->head;
	
	while (curr != NULL) {
		
		if (curr->val != NULL) {
		
			// Check all alien coord for all types of bullet hits
			if (hitAlien(p, curr->val->point)) {

				// Set alien dead flag
				curr->val->dead = true;
				return true;
			}
		}

		// Try next node in list
		curr = curr->next;
	}

	return false;
}

bool hitAlien(Point *p, Point *a) {
	
	int x1, y1;
	int x2, y2;
	int x3, y3;

	x1 = p->x - 1;
	y1 = p->y + 2;
	x2 = p->x;
	y2 = p->y + 1;
	x3 = p->x + 1;
	y3 = p->y;

	return (a->x == x1 && a->y == y1)
	   || (a->x == x2 && a->y == y1)
	   || (a->x == x3 && a->y == y1)
       || (a->x == x1 && a->y == y2)
	   || (a->x == x3 && a->y == y2)
	   || (a->x == x1 && a->y == y3)
	   || (a->x == x3 && a->y == y3);
}

void removeNode(pthread_t rThread, LList *list) {

	if (list == NULL || list->head == NULL)
		return;

	pthread_mutex_lock(&logger_mutex);
    writeToLog_threaded("Linked List", rThread, "Removed from List.");
    pthread_mutex_unlock(&logger_mutex);

	Node *curr = list->head;

	if (curr->thread == rThread) {
		
        Node *temp = curr;
		curr = curr->next;

        // Garbage collect rThread and free node
//        pthread_mutex_lock(&garbageThreads_mutex);
//        add(NULL, curr->thread, garbageThreads);
//        pthread_mutex_unlock(&garbageThreads_mutex);
        
        if (temp->val != NULL) {
            free(temp->val->point);
            free(temp->val);
        }
        free(temp);
		return;
	}

	Node *prev = curr;
	curr = curr->next;

	// Try every node looking for alien thread id
	while (curr != NULL) {
		
		if (curr->thread == rThread) {
			
			prev->next = curr->next;

            // Garbage collect rThread and free node
//            pthread_mutex_lock(&garbageThreads_mutex);
//            add(NULL, curr->thread, garbageThreads);
//            pthread_mutex_unlock(&garbageThreads_mutex);

            if (curr->val != NULL) {
                free(curr->val->point);
                free(curr->val);
            }
            free(curr);

			return;
		}

		prev = curr;
		curr = curr->next;
	}
}	

Node* getHead(LList *list) {

	if (list == NULL || list->head == NULL)
		return NULL;

	Node* head = list->head;
	list->head = list->head->next;

	return head;
}

void clearList(LList *list) {
	
	if (list == NULL)
		return;

	pthread_mutex_lock(&logger_mutex);
    writeToLog("Linked List", "Clearing list.");
    pthread_mutex_unlock(&logger_mutex);

	clearAux(list->head);
}

void clearAux(Node *curr) {

	if (curr != NULL) {
		clearAux(curr->next);
        pthread_mutex_lock(&garbageThreads_mutex);
        add(NULL, curr->thread, garbageThreads);
        pthread_mutex_lock(&garbageThreads_mutex);
        free(curr->val);
	    free(curr);
    }
}
