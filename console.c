/**********************************************************************
  Module: console.c
  Author: Jim Young
  Date:   2014 Jan 3

  Purpose: see console.h

  Changes: 
    2013 Jan 3 [ Jim Young ]
      - initial version
    2014 Jan 3 [ Jim Young ]
      - adopted to invaders

  NOTES: none
**********************************************************************/

#include "console.h"
#include "logger.h"
#include "globals.h"
#include <curses.h>
#include <string.h>
#include <time.h>        /*for nano sleep */


int SCR_WIDTH, SCR_HEIGHT;
int screenLock = FALSE;

/* Local functions */

static int checkScreenSize(int reqHeight, int reqWidth) {

  if ( (reqWidth > COLS) || (reqHeight > LINES) ) {
    fprintf(stderr, "\n\n\rSorry, your window is only %ix%i. \n\r%ix%i is required. Sorry.\n\r", COLS, LINES, reqWidth, reqHeight);
    return (FALSE);
  }

  return(TRUE);
}

int screenInit(int height, int width, char *image[]) { /* assumes image height/width is same as height param */
    int status;

    initscr();
    crmode();
    noecho();
    clear();

    SCR_HEIGHT = height;  SCR_WIDTH = width;
    status = checkScreenSize(SCR_HEIGHT, SCR_WIDTH);
    
    if (status) {
		screenDrawImage(0, 0, image, SCR_HEIGHT);
    		screenRefresh();
	  }
   
    return(status);
}

void screenDrawImage(int row, int col, char *image[], int height) {
    int i, length;
    int newLeft, newRight, newOffset, newLength;

    if (screenLock) return;

    for (i = 0; i < height; i++) {
		if (row+i < 0 || row+i >= SCR_HEIGHT)
			continue;
		length = strlen(image[i]);
		newLeft  = col < 0 ? 0 : col;
		newOffset = col < 0 ? -col : 0;
		newRight = col+length > SCR_WIDTH ? SCR_WIDTH : col+length;
		newLength = newRight - newLeft + 1;
		if (newOffset > length || newLeft >= newRight) 
		  continue;

		if (mvaddnstr(row+i, newLeft, image[i]+newOffset, newLength) == ERR) {
            pthread_mutex_lock(&logger_mutex);
			writeToLog("Console", "ERROR drawing to screen");
            pthread_mutex_lock(&logger_mutex);
        }
    }
}

void screenClearImage(int row, int col, int width, int height) {
    int i, j;
    if (screenLock) return;
      
    if (col+width > SCR_WIDTH)
	width = SCR_WIDTH-col+1;
    if (col < 0) {
	width += col; /* -= -col */
	col = 0;
    }

    if (width < 1 || col > SCR_WIDTH) /* nothing to clear */
	return;

    for (i = 0; i < height; i++) {
	if (row+height < 0 || row+height > SCR_HEIGHT)
			continue;
	move(row+i, col);
	for (j = 0; j < width; j++)
	    addch(' ');
    }
}

void screenRefresh(void) {
	if (!screenLock) {
	    move(LINES-1, COLS-1);
	    refresh();
	}
}

void screenFini(void) {
    endwin();
}

void putBanner(const char *str) {
  if (screenLock) return;
  int len;

  len = strlen(str);
  
  move (SCR_HEIGHT/2, (SCR_WIDTH-len)/2);
  addnstr(str, len);

  screenRefresh();
}

void putString(char *str, int row, int col, int maxlen) {
  if (screenLock) return;
  move(row, col);
  addnstr(str, maxlen);
}


/* setup to work in USECS, reduces risk of overflow */
#define TIMESLICE_USEC 10000
#define TIME_USECS_SIZE 1000000
#define USEC_TO_NSEC 1000  
struct timespec getTimeout(int ticks) {
  struct timespec rqtp;

  /* work in usecs at first */
  rqtp.tv_nsec = TIMESLICE_USEC * ticks;

  /* handle usec overflow */
  rqtp.tv_sec = rqtp.tv_nsec / TIME_USECS_SIZE;
  rqtp.tv_nsec %= TIME_USECS_SIZE;

  rqtp.tv_nsec *= USEC_TO_NSEC;  /*convert to nsecs */
  return rqtp;
}

void sleepTicks(int ticks) {

  if (ticks <= 0)
    return;

  struct timespec rqtp = getTimeout(ticks);
  nanosleep(&rqtp, NULL);
}

#define FINAL_PAUSE 5
void finalKeypress() {
	flushinp();
	sleepTicks(FINAL_PAUSE);
    	move(LINES-1, COLS-1);
	getch();
}

void disableScreen(int disabled) {
	screenLock = disabled;
}
