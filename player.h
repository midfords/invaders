#ifndef PLAYER_H
#define PLAYER_H

char *playerModel[2];

//
// playControllerThread
//   controlls all player movement, listens for keypress, and spawns threads on <space>
//
void *playerControllerThread();

#endif
