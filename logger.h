#ifndef LOGGER_H
#define LOGGER_H

#include "commons.h"
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

//
// file
//   Pointer to the current log file.
//
FILE *file;

//
// openLog
//   Open log file, if file does not exist, create it.
// @return whether or not successful
//
bool openLog(char* fileName);

//
// closeLog
//   Close the log file
// @return whether or not successful
//
bool closeLog();

//
// writeToLog_threaded
//    Writes message to log, with special thread annotation.
// @return whether or not successful
//
bool writeToLog_threaded(char* entity, pthread_t thread, char* message);

//
// writeToLog
//    Write message to log.
// @return whether or not successful
//
bool writeToLog(char* entity, char* message);

#endif