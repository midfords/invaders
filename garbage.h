#ifndef GARBAGE_H
#define GARBAGE_H

//
// garbageCollectionThread
//   Periodically runs and joins dead threads.
//
void *garbageCollectionThread();

#endif