#include "logger.h"
#include "commons.h"
#include "console.h"
#include "globals.h"
#include "player.h"
#include "bullet.h"
#include "llist.h"
#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>

#define PLAYER_MOVE_AMOUNT 1

void *playerControllerThread() {

	char move;			// Player's keypress
	Point *bulletPoint;	// Created and sent to bullet thread.
	
	// Initialize player to start in bottom center of screen
	pthread_mutex_lock(&player_mutex);
	p_point->x = COORD_X / 2 - 1;
	p_point->y = COORD_Y - 2;
	pthread_mutex_unlock(&player_mutex);

	// Draw player to screen initial
	pthread_mutex_lock(&image_mutex);
	pthread_mutex_lock(&player_mutex);
	screenDrawImage(p_point->y, p_point->x, playerModel, 2);
	pthread_mutex_unlock(&player_mutex);
	pthread_mutex_unlock(&image_mutex);

	pthread_mutex_lock(&logger_mutex);
	writeToLog("Player Controller", "Drew player model to screen.");
	pthread_mutex_unlock(&logger_mutex);

	while (*gameOver == false) {
		move = getchar();	// Get player keypress

		switch (move) {
		case 'a':	// Move left
			if (p_point->x - PLAYER_MOVE_AMOUNT >= 0) {
				pthread_mutex_lock(&player_mutex);
				pthread_mutex_lock(&image_mutex);
				screenClearImage(p_point->y, p_point->x, 2, 2);
				p_point->x -= PLAYER_MOVE_AMOUNT;
				screenDrawImage(p_point->y, p_point->x, playerModel, 2);
				pthread_mutex_unlock(&image_mutex);
				pthread_mutex_unlock(&player_mutex);

				pthread_mutex_lock(&logger_mutex);
				writeToLog("Player Controller", "Processed 'a' keypress.");
				pthread_mutex_unlock(&logger_mutex);
			}
			break;
		case 'd':	// Move right
			if (p_point->x + 2 < COORD_X) {
				pthread_mutex_lock(&player_mutex);
				pthread_mutex_lock(&image_mutex);
				screenClearImage(p_point->y, p_point->x, 2, 2);
				p_point->x += PLAYER_MOVE_AMOUNT;
				screenDrawImage(p_point->y, p_point->x, playerModel, 2);
				pthread_mutex_unlock(&image_mutex);
				pthread_mutex_unlock(&player_mutex);

				pthread_mutex_lock(&logger_mutex);
				writeToLog("Player Controller", "Processed 'd' keypress.");
				pthread_mutex_unlock(&logger_mutex);
			}
			break;
		case 'w':	// Move up
			if (p_point->y - PLAYER_MOVE_AMOUNT > (COORD_Y - 2 - SAFE_ZONE)) {
				pthread_mutex_lock(&player_mutex);
				pthread_mutex_lock(&image_mutex);
				screenClearImage(p_point->y, p_point->x, 2, 2);
				p_point->y -= PLAYER_MOVE_AMOUNT;
				screenDrawImage(p_point->y, p_point->x, playerModel, 2);
				pthread_mutex_unlock(&image_mutex);
				pthread_mutex_unlock(&player_mutex);

				pthread_mutex_lock(&logger_mutex);
				writeToLog("Player Controller", "Processed 'w' keypress.");
				pthread_mutex_unlock(&logger_mutex);
			}
			break;
		case 's':	// Move down
			if (p_point->y + 2 < COORD_Y) {
				pthread_mutex_lock(&player_mutex);
				pthread_mutex_lock(&image_mutex);
				screenClearImage(p_point->y, p_point->x, 2, 2);
				p_point->y += PLAYER_MOVE_AMOUNT;
				screenDrawImage(p_point->y, p_point->x, playerModel, 2);
				pthread_mutex_unlock(&image_mutex);
				pthread_mutex_unlock(&player_mutex);

				pthread_mutex_lock(&logger_mutex);
				writeToLog("Player Controller", "Processed 's' keypress.");
				pthread_mutex_unlock(&logger_mutex);
			}
			break;
		case ' ':	// Shoot bullet
			// Create copy of player coords for bullet's initial coords.
			bulletPoint = malloc(sizeof(Point));
			bulletPoint->x = p_point->x;
			bulletPoint->y = p_point->y;
			createPlayerBulletThread(bulletPoint);

			pthread_mutex_lock(&logger_mutex);
			writeToLog("Player Controller", "Processed ' ' keypress.");
			pthread_mutex_unlock(&logger_mutex);
			break;
		}
	}

	pthread_mutex_lock(&logger_mutex);
	writeToLog("Player Controller", "Exiting thread.");
	pthread_mutex_unlock(&logger_mutex);

	pthread_exit(NULL);
}
